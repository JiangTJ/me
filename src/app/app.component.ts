import { Component } from '@angular/core';
import {MatDialog} from '@angular/material';
import {AboutDialogComponent} from './about-dialog/about-dialog.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Mine☀';
  mine = {
    info: {
      title: '个人简介',
      description: '姜涛杰'
    },
    skill: {
      title: '技能&擅长',
      description: 'Java'
    },
    open: {
      title: '开源项目',
      description: ''
    }
  };

  hideName(): void {
    this.mine.info.description = '';
  }

  showName(): void {
    this.mine.info.description = '姜涛杰';
  }

  hideSkill(): void {
    this.mine.skill.description = '';
  }

  showSkill(): void {
    this.mine.skill.description = 'Java';
  }

  jumpToBlog(): void {
    window.location.href = 'https://jiangtj.gitlab.io';
  }
  constructor(public dialog: MatDialog) {
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(AboutDialogComponent, {
      width: '600px'
    });
    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }

}
